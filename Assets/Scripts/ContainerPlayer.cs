using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Audio;

public class ContainerPlayer : MonoBehaviour
{
    [Header("Containers avaliable to play")]
    public List<so_TrackContainer> containersAvailabesToPlay;    //Cargo todos los containers que quiera que est�n disponibles
    private so_TrackContainer currentContainer;

    [TooltipAttribute("Select this AudioMixer to override the Mixer ouput from the container")] 
    [SerializeField] private AudioMixerGroup overrideMixer;
    public Metronome sc_metronome;
    private AudioSource[] arr_audiosourcePlayAlways, arr_audioSourcesPlayVariable, arr_audiosourcePlayAlways2, arr_audioSourcesPlayVariable2;
    private double longestLoopClip;

    public AudioSource metronomeClick;
    private int alternateArrays =0;    //si es par toca unos si no otros
    [SerializeField] private GameObject go_audioSources;    //donde alojar los audiosources
    [SerializeField] private GameObject go_metronome;
    /*HAY QUE CAMBIAR LA ESTRATEGIA, CREAR UN ARRAY DE AUDIOSOURCES Y ASIGNARLES LOS CLIPS QUE SE VAN A TOCAR, ASIGNAR SUS OUPUTS AL MIXERGROUP
     UTILIZAR PLAY:SETSCHEDULE   */

    public void PlayContainer(string nameOfcontainer)
	{
        SelectContainer(nameOfcontainer);
        FillArrays();
        sc_metronome.SetBpm(currentContainer.bpm);
        sc_metronome.StartMetronomeCount();
    }
    private void FillArrays()
	{
        //Play variable section (loop)
        if (currentContainer.audioListPlayVariable.Count != 0)
		{
            arr_audioSourcesPlayVariable = new AudioSource[currentContainer.audioListPlayVariable.Count];
            int counterPlayVariable = 0;
            foreach (AudioClip clipInList in currentContainer.audioListPlayVariable)
            {
                arr_audioSourcesPlayVariable[counterPlayVariable].outputAudioMixerGroup = currentContainer.mixerGroup;
                arr_audioSourcesPlayVariable[counterPlayVariable].clip = clipInList;
                counterPlayVariable++;
            }
            arr_audioSourcesPlayVariable2 = arr_audioSourcesPlayVariable;
        }
        //Play always section (loop)
        if (currentContainer.audioListPlayAlways.Count != 0)
        {
            arr_audiosourcePlayAlways = new AudioSource[currentContainer.audioListPlayAlways.Count];
            arr_audiosourcePlayAlways2 = new AudioSource[currentContainer.audioListPlayAlways.Count];
            int counterPlayAlways = 0;
            longestLoopClip = 0;
            foreach (AudioClip clipInList in currentContainer.audioListPlayAlways)
            {
                AudioSource audiosS = go_audioSources.AddComponent<AudioSource>();
                AudioSource audiosS_2 = go_audioSources.AddComponent<AudioSource>();
                audiosS.playOnAwake = false;
                arr_audiosourcePlayAlways[counterPlayAlways] = audiosS;
                arr_audiosourcePlayAlways2[counterPlayAlways] = audiosS_2;
                arr_audiosourcePlayAlways[counterPlayAlways].outputAudioMixerGroup = currentContainer.mixerGroup;
                arr_audiosourcePlayAlways2[counterPlayAlways].outputAudioMixerGroup = currentContainer.mixerGroup;
                arr_audiosourcePlayAlways[counterPlayAlways].clip = clipInList;
                arr_audiosourcePlayAlways2[counterPlayAlways].clip = clipInList;
                double duration = clipInList.samples / clipInList.frequency;
                if (duration > longestLoopClip) longestLoopClip = duration;             //Esta es una manera mucho m�s precisa de calcular el tiempo de un clip
                counterPlayAlways++;
            }
        }
        alternateArrays = 0;
    }

    /*THESE METHODS HAVE TO BE PLAYED AFTER POPULATE THE ARRAYS*/
    public void PlayVariableSection(double scheduledTime, AudioSource[] arr_AudioSource)
	{
        switch (currentContainer.playMode){
            case "all":
                foreach (AudioSource sourceInArray in arr_audioSourcesPlayVariable)
                {
                    sourceInArray.PlayScheduled(scheduledTime);
                }
                break;
            case "random":
                break;
            case "sequence":
                break;
		}
    }       //TO DO: MODES OF PLAY

    public void PlayAlwaysSection(double scheduledTime, AudioSource[] arr_toPlay)     //hay que pasarle el scheduled time y de qu� array quiero que lo lea
	{
        Debug.Log("Play always section called, time: " + scheduledTime);
       foreach(AudioSource sourceInArray in arr_toPlay)
		{
            sourceInArray.PlayScheduled(scheduledTime);
		}
        alternateArrays++;
    }
    //voy a llamar a este metodo desde el metronomo, �ste gestionar� como una llave que desbloquear� lo que se tiene que tocar
    //no es el tempo real, el scheduledTime es un desfase hacia alante que me servir� para que las pistas se carguen
    public void PlayBar(double scheduledTime, int beatSegment)       
	{
        metronomeClick.PlayScheduled(scheduledTime);    
        Debug.Log("ContPlayer:scheduledTime " + scheduledTime +" dspTime:"+ AudioSettings.dspTime+" Bar="+ beatSegment);
		if (beatSegment == 4) //esto quiere decir que el siguiente ser�a tiempo fuerte del primer comp�s y habr�a que desbloquear
		{
            if (alternateArrays % 2 == 0) PlayAlwaysSection(scheduledTime, arr_audiosourcePlayAlways);
            else PlayAlwaysSection(scheduledTime, arr_audiosourcePlayAlways2);
		}
        Debug.Log("Alternate arrays: " + alternateArrays);
    }
    /*END OF PLAY METHODS*/
    public void SelectContainer(string namePlayContainer)
	{
        foreach(so_TrackContainer container in containersAvailabesToPlay)
		{
            if(container.name == namePlayContainer)
			{
                currentContainer = container;
			}
		}
	}

    private void ChangeMixer(AudioMixerGroup mixerGroup)
	{
        //if (!overrideMixer) audioSource.outputAudioMixerGroup = mixerGroup;
        //else audioSource.outputAudioMixerGroup = overrideMixer;
	}
}



