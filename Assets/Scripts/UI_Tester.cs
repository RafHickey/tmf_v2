﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_Tester : MonoBehaviour
{
	public TMP_InputField nameOfContainer;
	public ContainerPlayer sc_containerPlayer;

	public void PlayContainer()
	{
		string name = nameOfContainer.text.ToString();
		sc_containerPlayer.PlayContainer(name);
	}
}
