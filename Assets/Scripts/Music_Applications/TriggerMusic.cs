﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TriggerMusic : MonoBehaviour
{
	public ContainerPlayer_Manager container_manager;
	public string containerToPlay;
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			container_manager.PlayContainer(containerToPlay);
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			container_manager.StopMusic();		}
	}

	private void OnDrawGizmos()
	{
		Handles.Label(transform.position, containerToPlay.ToString() + "Trigger area");
	}
}
