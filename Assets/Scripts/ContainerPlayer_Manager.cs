﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerPlayer_Manager : MonoBehaviour
{
    [Header("Containers avaliable to play")]
    public List<so_TrackContainer> containersAvailabesToPlay;    //Los contenedores con las pistas
    private so_TrackContainer currentContainer;

    [Header("Gameobjects to alocate audiosources")]
    public GameObject go_audioSourcesAllocator;                    //Aquí es donde se añadirán los AudioSources necesarios
    [Header("Metronome")]
    public Metronome metronome;

    private ContainerPlayer_2 contPlayer;
    private ContainerPlayer_2 contPlayerMirror;     //Este duplica al contPlayer es para evitar tener que hacer un scheduled nuevo y detenga la reproducción
    private int alternator=1;                       //si es par que triggee un container si es impar otro
    private int alternator_variable = 1;
    private int indexOfVariableClip = -1;
    public double longestClipDuration;          //CAMBIAR ESTO A PRIVATE DESPUES DEL TEST
    private double globalEnd_longestCliptime;
    private double current_variableClipLength=0;  //almaceno para ver si tengo que reproducir el siguiente de la secuencia, o sacar otro random
    private bool stopClips = false;             //parar los clips en el siguiente beat
    private bool playStingerIn = false;
    private bool playsAfterContainer=false;

    public Custom_Music_Events metronome_events;
	private void OnEnable()
	{
        metronome_events.onChangeBar += Bar_Event_Listener;
        metronome_events.onChangeBeat += Beat_Event_Listener;
    }
	private void OnDisable()
	{
        metronome_events.onChangeBar -= Bar_Event_Listener;
        metronome_events.onChangeBeat -= Beat_Event_Listener;
    }

    public void StopMusic()
	{
        stopClips = true;
        
	}
    public void PlayContainer(string containerToPlay)
	{
        
		if (!playsAfterContainer)
		{
            CreateContainerAuxNeeds(containerToPlay);
            metronome.StartMetronomeCount();
		}
		else
		{
            stopClips = true;
            StartCoroutine(Wait_CreateNewContainerPlayers(60/(currentContainer.bpm) *4 , containerToPlay));
            //AQUI HAY UN PROBLEMA, LA CORUTINA TIENE QUE ESPERAR 
		}
	}
    /* Se le pasa el nombre del SO con los contenederos que se pueden reproducir, 
     -Selecciona el container y lo guarda en current container
    -Crea un containerPlayerMirror, que es una copia
     -Manda al metrónomo el tempo
     -Recoge el clip de mayor longitud (globalEnd_longestCliptime) */
    public void CreateContainerAuxNeeds(string nameOfContainer)  //ES LO QUE REALMENTE GENERA LA MUSICA
	{
        foreach (so_TrackContainer container in containersAvailabesToPlay)      //selecciona el container correspondiente
        {
            if (container.name == nameOfContainer)
            {
                currentContainer = container;
                contPlayer = CreateContainerPlayer();
                contPlayer.SetContainerProperties();    //manda las propiedades al colaborador para que construya lo necesario
                CreateMirrorContainer();            //creamos la copia
            }
        }
        SendPropertiesToMetronome(metronome, currentContainer);     //manda las propiedades al metrónomo
        longestClipDuration = contPlayer.ReturnLongestClipTime();       //este longest clip es sólo de la parte de play always
        playStingerIn = true;
    }
    
    private void Beat_Event_Listener()
	{
     
	}
    /* Este método escucha el evento que lanza el metrónomo, recogiendo un valor que situado en la línea de tiempo representa el próximo
     cambio de compás llamado "evento" (scheduledEvent)
    -Checkea en la linea de tiempo si en el próximo evento habrá terminado la reproducción del clip más largo globalEnd_longestCliptime
    - Manda al ContainerPlayer2 la instrucción de Play_clips(scheduledEvent)*/
    public void Bar_Event_Listener(double scheduledEvent)   
	{
		if (stopClips)
		{
            StingerOut(scheduledEvent);
            contPlayer.Stop_Clips(scheduledEvent);
            contPlayerMirror.Stop_Clips(scheduledEvent);
            metronome.StopMetronomeCount();
            stopClips = false;              //una vez parados podemos volver a ponerlo en su estado inicial
            return;
        }
        if (scheduledEvent >= globalEnd_longestCliptime)
        {
            StingerIn(scheduledEvent);
			if (alternator % 2 == 0)            //si es par toca uno si no otro
			{
                contPlayer.Play_Clips(scheduledEvent);
			}
			else
			{
                contPlayerMirror.Play_Clips(scheduledEvent);
            }
            globalEnd_longestCliptime = longestClipDuration + AudioSettings.dspTime; //hay que actualizar el nuevo globalEnd_longestClipTime;
            if (current_variableClipLength == 0) current_variableClipLength = globalEnd_longestCliptime;
            alternator++;
        }
        if((scheduledEvent>= current_variableClipLength) && currentContainer.audioListPlayVariable.Count!=0)
		{
            PlayNewVariableClip(scheduledEvent);
		}
    }
    private void PlayNewVariableClip(double schedTime)
	{
        if (alternator_variable % 2 == 0)            //si es par toca uno si no otro
        {
            contPlayer.Play_Clips_Variable(schedTime,CheckPlayMode());
        }
        else
        {
            contPlayerMirror.Play_Clips_Variable(schedTime, CheckPlayMode());
        }
        current_variableClipLength = contPlayer.ReturnVariableClipLength(indexOfVariableClip) + AudioSettings.dspTime;
        alternator_variable++;
    }
    /*Esta funcion devuelve un índice según el modo de reproducción*/
    private int CheckPlayMode()
	{
        switch (currentContainer.playMode)
		{
            case "random":
                indexOfVariableClip = Random.Range(0,currentContainer.audioListPlayVariable.Count-1);
                break;
            case "sequence":
                indexOfVariableClip++;
                if (indexOfVariableClip> currentContainer.audioListPlayVariable.Count-1)
				{
                    indexOfVariableClip = 0;
				}
                break;
        }
        return indexOfVariableClip;
	}
    /*Corutina para dejar que acabe los clips y cargar el nuevo container*/
    private IEnumerator Wait_CreateNewContainerPlayers(float waitTime, string newContainer)
    {
        yield return new WaitForSeconds(waitTime);
        CreateContainerAuxNeeds(newContainer);
        metronome.StartMetronomeCount();
    }
    /*Crea la instancia del container_player2 y la devuelve, funciones del container_player2:
    -Se encarga de crear todos los AudioSources necesarios
    -Asigna los Clips a los AudioSources
    -Recibe el siguiente evento (scheduledTime) y se lo aplica a los AudioSources*/
    private ContainerPlayer_2 CreateContainerPlayer()
	{
        ContainerPlayer_2 containerPlayer = new ContainerPlayer_2(currentContainer, go_audioSourcesAllocator);
        return containerPlayer;
	}
    /*Manda al metrónomo los BPM*/
    private void SendPropertiesToMetronome(Metronome metron, so_TrackContainer container)
    {
        metron.SetBpm(container.bpm);
    }
    private void CreateMirrorContainer()
	{
        contPlayerMirror = CreateContainerPlayer();
        contPlayerMirror.SetContainerProperties();      //mando las propiedades al container, no hace falta mandarselas al metrónomo también
	}
    private void StingerIn(double schedTime)
	{
		if (playStingerIn && currentContainer.stingerIn !=null)
		{
            double scheduledStinger = schedTime - currentContainer.timeIn;
            contPlayer.PlayStinger(scheduledStinger, currentContainer.stingerIn);
            playStingerIn = false;
        }
	}
    private void StingerOut(double schedTime)
    {
		if (currentContainer.stingerOut != null)
		{
            double scheduledStinger = schedTime - currentContainer.timeOut;
            contPlayer.PlayStinger(scheduledStinger, currentContainer.stingerOut);
            playStingerIn = false;
        }
    }
    
}


    
    
