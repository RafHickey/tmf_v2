﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;



public class ContainerPlayer_2 : MonoBehaviour
{
	private so_TrackContainer container;
	private GameObject audioSourcesAllocator_go;
	private List<AudioSource> l_playAlways_AudioSources;
	private List<AudioSource> l_playVariable_AudioSources;
	private bool variablePlayed = false;
	private double longestClip;
	

	/*Constructor para decirle el contenedor actual y dónde tiene que generar los AudioSources*/
	public ContainerPlayer_2(so_TrackContainer containerSelected, GameObject go_audioSources){
		container = containerSelected;
		audioSourcesAllocator_go = go_audioSources;
	}
	/*Crea un colaborador llamado AudioSources_Creator, funciones:
	 -Crear los audiosources correspondientes
	 -Devolver la duración del clip más larga de la lista de "PlayAlways"*/
	public void SetContainerProperties()	
	{
		AudioSources_Creator utility_AS = new AudioSources_Creator();
		l_playAlways_AudioSources = utility_AS.CreateAudioSources(audioSourcesAllocator_go, container.audioListPlayAlways, container.mixerGroup);
		l_playVariable_AudioSources = utility_AS.CreateAudioSources(audioSourcesAllocator_go, container.audioListPlayVariable, container.mixerGroup);
		longestClip = utility_AS.GetLongestClipTime(l_playAlways_AudioSources);
	}

	/*Asigna el momento en el que se tienen que reproducir en la linea temporal del dsp*/
	
	public void Play_Clips(double scheduledTime)
	{
			foreach (AudioSource source in l_playAlways_AudioSources)
			{
				source.PlayScheduled(scheduledTime);
			}
	}
	public void Play_Clips_Variable(double scheduledTime, int index)
	{
		AudioSource source = l_playVariable_AudioSources[index];
		source.PlayScheduled(scheduledTime);
	}
	public void PlayStinger(double schedTime, AudioClip stinger)
	{
		AudioSource stingerSource = audioSourcesAllocator_go.AddComponent<AudioSource>();
		stingerSource.clip = stinger;
		stingerSource.outputAudioMixerGroup = container.mixerGroup;
		stingerSource.PlayScheduled(schedTime);
		AudioSources_Creator utility_AS = new AudioSources_Creator();
		utility_AS.DestroyStinger(stingerSource, stinger.length);
	}
	public void Stop_Clips(double scheduledTime)
	{
		foreach (AudioSource source in l_playAlways_AudioSources)
		{
			source.SetScheduledEndTime(scheduledTime);
		}
		foreach (AudioSource source in l_playVariable_AudioSources)
		{
			source.SetScheduledEndTime(scheduledTime);
		}
		AudioSources_Creator utility_AS = new AudioSources_Creator();
		double timeToDestroy = scheduledTime - AudioSettings.dspTime;
		utility_AS.DestroyAudioSources(l_playAlways_AudioSources, (float)timeToDestroy);
		utility_AS.DestroyAudioSources(l_playVariable_AudioSources, (float)timeToDestroy);
	}
	public double ReturnVariableClipLength(int index)	//le paso el índice de la lista o array
	{
		if (l_playVariable_AudioSources[index] == null) Debug.LogError("Try to get length of inexistent clip in" + l_playVariable_AudioSources);
		AudioSource temSource = l_playVariable_AudioSources[index];
		double duration = (double)temSource.clip.samples / temSource.clip.frequency;  //para sacar la duración del clip de una manera más exacta
		return duration;
	}
	/*Devuelve la duración del clip más largo*/
	public double ReturnLongestClipTime()
	{
		if (longestClip != 0) return longestClip;
		else return 0;
	}
}

/*Clase colaboradora para crear los AudioSources*/
public class AudioSources_Creator: MonoBehaviour
{

	/*-Crea una lista de AudioSources
	 -Añade los clips necesarios a los AudioSources de la lista*/
	public List<AudioSource> CreateAudioSources(GameObject audioSources_location, List<AudioClip> list_clips, AudioMixerGroup mixer)
	{
		
		List<AudioSource> audioSources_list = new List<AudioSource>();      //creo lista de audiosources según el número de clips
		int i = 0;
		foreach (AudioClip clip in list_clips)
		{
			AudioSource tempSource = audioSources_location.AddComponent<AudioSource>();  //añado el Asource en el gameobject
			tempSource.playOnAwake = false;
			audioSources_list.Add(tempSource);
			audioSources_list[i].clip = clip;
			audioSources_list[i].outputAudioMixerGroup = mixer;
			i++;
		}
		return audioSources_list;
	}
	/*Devuelve la duración del clip más largo de la lista*/
	public float GetLongestClipTime(List<AudioSource> list_sources)
	{
		float longestClip=0;
		foreach(AudioSource source in list_sources)
		{
			double duration = (double)source.clip.samples / source.clip.frequency;  //para sacar la duraciónd el clip de una manera más exacta
			if (duration > longestClip)
			{
				longestClip = source.clip.length;
			}
			
		}
		return longestClip;
	} 

	public void DestroyAudioSources(List<AudioSource> list_sources, float waitToDestroy)
	{
		foreach (AudioSource source in list_sources)
		{
			Destroy(source, waitToDestroy);
		}
	}

	public void DestroyStinger(AudioSource source, float waitToDestroy)
	{
		Destroy(source, waitToDestroy);
	}

}
