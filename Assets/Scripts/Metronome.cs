﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using TMPro;

public class Metronome : MonoBehaviour
{
    [SerializeField] private float bpm;
    [SerializeField] private double beatDuration;
    private int beatCount=4;
    private double beatTimeCounter;
    [SerializeField] private double barDuration;
    private int barCount = 0;
    private double startTime;           //es el tiempo que representa cuando empieza a contar el beat
    private double elapsedTimeInit;
    [SerializeField] private double nextEventTime;       //momento en la linea de tiempo absoluta en el que pasa el evento 
    private bool running = false;
    /*TEST*/
    public AudioSource as_click;
    [Header("UI Test Settings")]
    public Custom_Music_Events music_events;
    public TextMeshProUGUI tmpro_Bpm;
    public TextMeshProUGUI tmpro_BarCount;
    public TextMeshProUGUI tmpro_BeatCount;
    public TextMeshProUGUI tmpro_BarDuration;
    public TextMeshProUGUI tmpro_BeatDuration;
    public TextMeshProUGUI tmpro_dspTime;
    public TextMeshProUGUI tmpro_sampleRateProcessing;
    public TextMeshProUGUI tmpro_nextEventTime;
    public TextMeshProUGUI tmpro_longestClip;
    public TextMeshProUGUI tmpro_remainder;
    public TextMeshProUGUI tmpro_startTime;

    public ContainerPlayer_Manager cont_player_TEST;

    private void Start()
	{
        //SetBpm(80); testeo
        float sampleRate = AudioSettings.outputSampleRate/1000;
        tmpro_sampleRateProcessing.text = sampleRate.ToString() + "KHz";
        
	}

	public void StartMetronomeCount()
    {
        as_click = gameObject.GetComponent<AudioSource>();      //aqui está el sonido del CLICK
        startTime = AudioSettings.dspTime;  //tiempo en el que empieza el metrónomo
        tmpro_startTime.text = startTime.ToString();
        nextEventTime = startTime+4*beatDuration;
        beatTimeCounter = startTime;
        elapsedTimeInit = AudioSettings.dspTime - startTime; //tiempo transcurrido desde el inicio hasta este momento
        running = true;
    }
    public void StopMetronomeCount()
	{
        running = false;
	}

    void Update()
    {
        if (!running)
        {
            return;
        }
        tmpro_dspTime.text = AudioSettings.dspTime.ToString();

        beatTimeCounter = AudioSettings.dspTime -startTime;     //el starttime se va actualizando
        if (beatTimeCounter > beatDuration){
            CalculateNextEvent(beatCount);
            beatCount++;
            if (beatCount > 4)
            {
                beatCount = 1;
                barCount++;
                tmpro_BarCount.text = barCount.ToString();
			}
            tmpro_BeatCount.text = beatCount.ToString();
        }
    }

    private void CalculateNextEvent(int beatCounter)
    {
                double clipDuration = cont_player_TEST.longestClipDuration;
                tmpro_longestClip.text = clipDuration.ToString();   //mostrar en pantalla la duraciónd el clip
        double remainder = (AudioSettings.dspTime - startTime) % beatDuration;           // este es el tiempo restante, porque le da igual el cociente que son las bars que le quedan por dar, le importa el resto
        //double remainder = CalculateRemainder(AudioSettings.dspTime, nextEventTime);    //aqui nextEvent time todavia es el anterior
        tmpro_remainder.text = remainder.ToString();
        //double timeToNextBeat = beatDuration - remainder;     //lo que queda hasta llegar a la siguiente bar, menos el desfase que lleva
        nextEventTime = AudioSettings.dspTime + beatDuration - remainder;
        tmpro_nextEventTime.text = nextEventTime.ToString() + "s";
        PlayClick(nextEventTime);
        music_events.ChangeBeat();
        if (beatCounter == 4)
        {
            music_events.Changebar(nextEventTime);   //este evento está aquí porque tiene que mandar el scheduled
        }
        startTime = nextEventTime;
    }

    private void PlayClick(double scheduled)
	{
        as_click.PlayScheduled(scheduled);
    }
    public void SetBpm(float tempo)
    {
        bpm = tempo;
        tmpro_Bpm.text = bpm.ToString();
        beatDuration = 60d / bpm;      // así estoy sacando cuantos segundos dura un beat
        tmpro_BeatDuration.text = beatDuration.ToString() + "s";
        barDuration = (60d / bpm) *4;   //de momento solo tiempos de 4/4 por eso el 4
        tmpro_BarDuration.text = barDuration.ToString() + "s";
    }
}