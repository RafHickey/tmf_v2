﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Custom_Music_Events : MonoBehaviour
{
	public static Custom_Music_Events MusicEvents;

	private void Awake()
	{
		MusicEvents = this;
	}

	public event Action<double> onChangeBar;
	public event Action onChangeBeat;
	public void Changebar(double nextEventTime)
	{
		if (onChangeBar != null)
		{
			//Debug.Log("ChangeBar on event"+ nextEventTime);
			onChangeBar(nextEventTime);
		}
	}
	public void ChangeBeat()
	{
		if (onChangeBeat != null)
		{
			onChangeBeat();
		}
	}

}
