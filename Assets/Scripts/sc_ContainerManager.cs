﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
//Esta clase la va a llamar el editorwindow container manager, guarda las funciones de lectura
public class sc_ContainerManager : MonoBehaviour
{
public List<so_TrackContainer> trackContainersList;    
private so_TrackContainer currentTrackContainer;

    public so_ContainerListData GetListOfContainersSO(){
        if(Resources.Load<so_ContainerListData>("TrackContainerList/TrackContainerList")){
            return Resources.Load<so_ContainerListData>("TrackContainerList/TrackContainerList");
        }
        else{
            throw new System.ArgumentException("Container Data Not Found");
        }
    }
    public List<so_TrackContainer> ReturnContainersInContainerList_SO(){
    so_ContainerListData containerListData = GetListOfContainersSO();
    return containerListData.GetListOfContainers();
    }

    public void SetCurrentTrackContainer(so_TrackContainer container){
        currentTrackContainer = container;
    }

}


