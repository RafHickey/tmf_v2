﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Audio;
public enum playMode{ sequence, random }
public class Editor_TrackContainer : EditorWindow
{
    private so_TrackContainer containerTrack;

    public playMode pm;
    private int removeIndex;
    private void OnGUI() {
        if (GUILayout.Button("Close")) {
            Close();
        }
        PaintContainerSettings();
        PaintClipsOfContainer();
        PaintClipsOfPlayAlways();
        PaintStingers();
    }


    public void SetCurrentContainer(so_TrackContainer container) {
        containerTrack = container;
    }
    public void PaintContainerSettings() {
        pm = (playMode)EditorGUILayout.EnumPopup("Play mode:", pm);
        containerTrack.SetPlayMode(pm.ToString());
        EditorGUILayout.BeginHorizontal("box");
        AudioMixerGroup newMixer;
        newMixer = EditorGUILayout.ObjectField("AudioMixer", containerTrack.mixerGroup, typeof(AudioMixerGroup), false) as AudioMixerGroup;
        containerTrack.bpm = EditorGUILayout.FloatField("BPM", containerTrack.bpm);
        if (GUI.changed) containerTrack.mixerGroup = newMixer;
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.LabelField("------Audioclips:  play variable section -----");
    }
    /*Aqui se pintan los clips del container*/
    public void PaintClipsOfContainer() {

        int counter = 0;
        List<AudioClip> tempArray = containerTrack.audioListPlayVariable;
        if (tempArray.Count != 0) //Avoid exception erros
        {
            foreach (AudioClip clip in containerTrack.audioListPlayVariable)
            {
                EditorGUILayout.BeginHorizontal("box");
                AudioClip tempClip;
                tempClip = EditorGUILayout.ObjectField("Track " + counter.ToString(), containerTrack.audioListPlayVariable[counter], typeof(AudioClip), false) as AudioClip;
                EditorGUILayout.EndHorizontal();
                if (GUI.changed)
                {
                    tempArray[counter] = tempClip;
                }
                counter++;
            }
        }
        EditorGUILayout.BeginHorizontal();

        int index = new int();
        index = EditorGUILayout.IntField("Index", index);
        if (GUI.changed) removeIndex = index;
        if (GUILayout.Button("RemoveClip"))
        {
            containerTrack.audioListPlayVariable.RemoveAt(removeIndex);
        }
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Add new clip to play variable section")) {
            containerTrack.audioListPlayVariable.Add(null);
        }
    }
    public void PaintClipsOfPlayAlways()
    {
        EditorGUILayout.LabelField("------Audioclips:  play always section -----");
        int counter = 0;
        List<AudioClip> tempArray = containerTrack.audioListPlayAlways;
        if (containerTrack.audioListPlayAlways.Count != 0)       //Avoid exception erros
		{
            foreach (AudioClip clip in containerTrack.audioListPlayAlways)
            {
                EditorGUILayout.BeginHorizontal("box");
                AudioClip tempClip;
                tempClip = EditorGUILayout.ObjectField("Track " + counter.ToString(), containerTrack.audioListPlayAlways[counter], typeof(AudioClip), false) as AudioClip;
                EditorGUILayout.EndHorizontal();
                if (GUI.changed)
                {                        //updates window
                    tempArray[counter] = tempClip;
                }
                counter++;
            }
        }
        EditorGUILayout.BeginHorizontal("box");
        if (GUILayout.Button("Remove last clip"))
        {
            containerTrack.audioListPlayAlways.RemoveAt(containerTrack.audioListPlayAlways.Count-1);
        }
        if (GUILayout.Button("Add new clip to play always section"))
        {
            containerTrack.audioListPlayAlways.Add(null);
        }
        EditorGUILayout.EndHorizontal();
    }
    public void PaintStingers()
    {
            EditorGUILayout.BeginHorizontal("box");
        AudioClip tempStingerIn;
        tempStingerIn = EditorGUILayout.ObjectField("StingerIn ", containerTrack.stingerIn, typeof(AudioClip), false) as AudioClip;
        containerTrack.timeIn = EditorGUILayout.FloatField("Time Stinger beat", containerTrack.timeIn);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal("box");
        AudioClip tempStingerOut;
        tempStingerOut = EditorGUILayout.ObjectField("StingerOut ", containerTrack.stingerOut, typeof(AudioClip), false) as AudioClip;
        containerTrack.timeOut = EditorGUILayout.FloatField("Time Stinger beat", containerTrack.timeOut);
        EditorGUILayout.EndHorizontal();
        if (GUI.changed)
        {                        //updates window
            containerTrack.stingerIn = tempStingerIn;
            containerTrack.stingerOut = tempStingerOut;
        }
    }
         
}
