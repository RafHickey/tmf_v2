﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


 public class Editor_ContainerManager : EditorWindow
{
    private sc_ContainerManager sc_ContainerManager;
    private string newContainerName;
    private string newContainerName_Fixed;
    [MenuItem("Adaptative Music/ContainerManager")]

    public static void ShowWindow(){
        CreateInstance<Editor_ContainerManager>().Show();
        //GetWindow<Editor_ContainerManager>("Container Manager");
    }

    private void OnEnable() {
    sc_ContainerManager = new sc_ContainerManager();
    }
    public void OnGUI() {
        EditorGUILayout.BeginHorizontal("box");
        newContainerName = EditorGUILayout.TextField("nameofContainer");
        if (GUI.changed) newContainerName_Fixed = newContainerName;
            if (GUILayout.Button("Create Container")){
            Debug.Log("Name in button::" + newContainerName_Fixed);
            CreateTrackContainer(newContainerName_Fixed);
            }
        if (GUILayout.Button("Close"))
        {
            Close();
        }
        EditorGUILayout.EndHorizontal();
    PaintAllContainers(Resources.Load<so_ContainerListData>("TrackContainerList/TrackContainerList"));
    }
    /*Método para generar un nuevo contenedor*/
    public void CreateTrackContainer(string nameContainer){         
        string path = "Assets/Resources/TrackContainers/" + nameContainer +  ".asset";
        so_TrackContainer newContainer;
        newContainer =new so_TrackContainer();
        AssetDatabase.CreateAsset(newContainer, path);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    /*Método para borrar el contenedor segun el nombre, el nombre se le pasa desde el botón*/
    private void DeleteContainer(string nameContainer){
        Debug.Log("Name in function::" + nameContainer);
        string path = "Assets/Resources/TrackContainers/" + nameContainer +  ".asset";
        AssetDatabase.DeleteAsset(path);
    }
    private void PaintAllContainers(so_ContainerListData containerListData){    
    containerListData.ClearList();
    containerListData.GetContainers();
        
    foreach(so_TrackContainer container in containerListData.GetListOfContainers()){
        GUILayout.BeginHorizontal("box");
        string containerName = container.name;
        if(GUILayout.Button(containerName)){        //Con este boton genero una nueva ventana en la que se carga en track Container
            sc_ContainerManager.SetCurrentTrackContainer(container);  
            Editor_TrackContainer newEditorTrackContainer = new Editor_TrackContainer() ;
                GUIContent newTitle = new GUIContent();
            newTitle.text = container.name;
            newEditorTrackContainer.titleContent= newTitle;
            newEditorTrackContainer.SetCurrentContainer(container);
            GetWindow(typeof(Editor_TrackContainer), newEditorTrackContainer);           
            }
        if(GUILayout.Button("Delete")){
            DeleteContainer(container.name);
            }
        GUILayout.EndHorizontal();
        }
    }
}
 