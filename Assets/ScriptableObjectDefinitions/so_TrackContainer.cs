﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEditor;

[CreateAssetMenu(fileName = "Track Container", menuName = "Adaptative Music/Create Track Container", order = 2)]
public class so_TrackContainer : ScriptableObject
{
    public float bpm;   
    public List<AudioClip> audioListPlayVariable;
    public List<AudioClip> audioListPlayAlways;
    public AudioClip stingerIn;
    public AudioClip stingerOut;
    public float timeIn;
    public float timeOut;
    public AudioMixerGroup mixerGroup;
    public string playMode ;

    public void SetPlayMode(string mode){
        playMode = mode;
    }
}

//Antes la usaba ahora no
public class PathContainerConverter{

    public string GetContainerAbsolutePath()
    {
        string absolutepath = Application.dataPath;     
        if (absolutepath.StartsWith(Application.dataPath))
        {
            string relativepath=  "Assets" + absolutepath.Substring(Application.dataPath.Length);
            string containerPath = relativepath + "/Resources/Containers";
        return containerPath;
        }
        else
        {
        Debug.Log("Container: no absolute path to get");
        return null;
        }
    }
}
