﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "Track List Data", menuName = "Adaptative Music/Create Track List Data", order = 1)]
public class so_ContainerListData : ScriptableObject
{
    
    [SerializeField] private List<so_TrackContainer> list_SoTrackContainers;

    public void AddNewContainerToList(so_TrackContainer newTrackContainer){
        list_SoTrackContainers.Add(newTrackContainer);
    }
    public void DeleteContainerFromList(so_TrackContainer deleteTrackContainer){
        list_SoTrackContainers.Remove(deleteTrackContainer);
    }
    public void ClearList(){
        list_SoTrackContainers.Clear();
    }

    public List<so_TrackContainer> GetListOfContainers(){
        return list_SoTrackContainers;
    }
    public void GetContainers(){
            so_ContainerListData containerData =Resources.Load<so_ContainerListData>("TrackContainerList/TrackContainerList");
            so_TrackContainer[] tempList =Resources.LoadAll<so_TrackContainer>("TrackContainers");
            List<so_TrackContainer> currentList = containerData.GetListOfContainers();
            foreach(so_TrackContainer containerItem in tempList){
                if(!currentList.Contains(containerItem)) {
                containerData.AddNewContainerToList(containerItem);
                }
            }

    }

[CustomEditor(typeof(so_ContainerListData))]
public class so_ContainerListData_Inspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if(GUILayout.Button("Get containers")){ 
            //GetContainers();
            so_ContainerListData dataList = (so_ContainerListData)target;
            dataList.GetContainers();
            //Metronome mymetronome = (Metronome)target;
        }

        if(GUILayout.Button("Clear container list")){ 
        so_ContainerListData containerData =Resources.Load<so_ContainerListData>("TrackContainerList/TrackContainerList");
        containerData.ClearList();
        }
    }

    
    }
}
